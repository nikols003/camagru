<?php

namespace Library;


abstract class Controller
{
	protected $content = "";

    private static $layout = '../view/layout.php';

//    public static function setAdminLayout()
//    {
//        self::$layout = 'admin_layout.phtml';
//    }

    protected function render($viewName, array $args = array())
    {
        extract($args);

		ob_start();
		if ($viewName != '')
        	require $viewName;
        $page = isset($page) ? $page : "";
        $content = $page."<br>".ob_get_clean();
		ob_start();
        require self::$layout;
		return ob_get_clean();
    }

    public static function renderError($message)
    {
        $content = $message;
        ob_start();
        require self::$layout;
        return ob_get_clean();
    }
}