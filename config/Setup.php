<?php

namespace config;


use Library\DbConnection;

class Setup
{
	public function __construct()
	{
		require_once 'database.php';
		$conn = mysqli_connect($DB_DSN, $DB_USER, $DB_PASSWORD);
		$sql = "CREATE DATABASE test";
		if (mysqli_query($conn, $sql)) {
			$db = DbConnection::getInstance()->getPdo();
			$q = $db->query("
				CREATE TABLE `comment` (
				  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
				  `author_name` varchar(64) DEFAULT NULL,
				  `description` varchar(9999) DEFAULT NULL,
				  `time` datetime DEFAULT NULL,
				  `photo_id` int(11) DEFAULT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
				
				CREATE TABLE `photo` (
				  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
				  `user_id` int(11) DEFAULT NULL,
				  `img` varchar(255) DEFAULT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;
				
				CREATE TABLE `user` (
				  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL, 
				  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `check_email` tinyint(1) DEFAULT '0'
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);
  
  ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

");
			$q->execute();
		}
		mysqli_close($conn);
	}

}



// Create database





?>