<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.04.2017
 * Time: 21:35
 */

namespace controller;


use Library\Controller;
use Library\DbConnection;
use Library\Request;
use Library\Session;
use model\UserModel;

class PhotoController extends Controller
{
	public function __construct(Request $request)
	{
		$array = array();
		if ($request->get('user'))
		{
			if ($request->post('submit') == 'comment')
				$this->add_comment($request);
			elseif ($request->post('submit') == 'delete_photo')
				$this->delete_photo($request->post('photo_id'));
			elseif ($request->post('submit') == 'delete_comment')
				$this->delete_comment($request->post('comment_id'));
			$array['big_photo'] = $this->take_all_user_photo($request->get('user'));
			$array['users'] = UserModel::take_all();
			$array['comments'] = $this->take_all_comments();
			$this->content = $this->render('', $array);
		}
		else
		{
			if ($request->post('submit') == 'Send File')
				$array['page'] = $this->save_from_web($request);
			if ($request->files('photo'))
				$array['page'] = $this->download($request);
			$array['photo'] = $this->take_all_user_photo(Session::get('login'));
			$this->content = $this->render('../view/download.php', $array);
		}
	}

	public function take_all_user_photo($email)
	{
		$img = array();
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->prepare('SELECT * FROM user JOIN photo ON user.id = photo.user_id WHERE email = :email');
		$ths->execute(array('email' => $email));
		$user = $ths->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($user as $item)
			$img[$item['id']] = $item['img'];
		return $img;
	}

	public function take_all_comments()
	{
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->query('SELECT * FROM comment');
		$ths->execute();
		$comments = $ths->fetchAll(\PDO::FETCH_ASSOC);
		return $comments;
	}


	public function save_from_web(Request $request)
	{
		$file = $request->post('photo');
		$file_name = UserModel::randomPassword().".png";
		$filedir = preg_split('/@/', Session::get('login'));
		if (!file_exists('img/'.$filedir[0]))
			mkdir('img/'.$filedir[0]);
		$file = preg_split('#data:image/png;base64,#', $file);
		file_put_contents('img/'.$filedir[0].'/'.$file_name, base64_decode($file[1]));
		return UserModel::add_photo_to_user($file_name);
	}

	public function download(Request $request)
	{
		$file = $_FILES['photo'];
		$file_name = $file['name'];
		$filedir = preg_split('/@/', Session::get('login'));
		if (!file_exists('img/'.$filedir[0]))
			mkdir('img/'.$filedir[0]);
		$type = $file['type'];
		if ($type != 'image/png' && $type != 'image/jpeg' && $type != 'iamge/jpg' && $type != 'image/gif')
			return array('page' => "Incorrect type of file!");
		move_uploaded_file($file['tmp_name'], 'img/'.$filedir[0].'/'.$file_name);
		return UserModel::add_photo_to_user($file_name);
	}

	public function add_comment(Request $request)
	{
			date_default_timezone_set('Europe/Kiev');
			$db = DbConnection::getInstance()->getPdo();
			$ths = $db->prepare('INSERT INTO comment (photo_id, author_name, description, time) VALUES (:photo_id, :author_name, :description, :time)');
			$ths->execute(array('photo_id' => $_POST['photo_id'], 'author_name' => $_SESSION['login'], 'description'
			=> $_POST['description'], 'time' => date('Y-m-d H:i:s')));
			$ths = $db->prepare('SELECT * FROM user JOIN photo ON user.id = photo.user_id WHERE photo.id = :id');
			$ths->execute(array('id' => $_POST['photo_id']));
			$user = $ths->fetch(\PDO::FETCH_ASSOC);

			if ($user['email'] != $_SESSION['login']) {
				$message = "Hi " . $user['first_name'] . ", user " . $_SESSION['login'] . ' added comment for your photo #'
					. $_POST['photo_id'] . ", go and read it!!!";
				mail($user['email'], "New comment!", $message);
			}
	}

	public function delete_photo($id)
	{
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->prepare("SELECT * FROM photo WHERE id = :id");
		$ths->execute(array('id' => $id));
		$photo = $ths->fetch(\PDO::FETCH_ASSOC);
		unlink("../".$photo['img']);
		$ths = $db->prepare("DELETE FROM photo WHERE id = :id");
		$ths->execute(array('id' => $id));
	}

	public function delete_comment($id)
	{
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->prepare("DELETE FROM comment WHERE id = :id");
		$ths->execute(array('id' => $id));
	}

	public function __toString()
	{
		return $this->content;
	}
}