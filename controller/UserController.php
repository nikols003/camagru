<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.04.2017
 * Time: 22:24
 */

namespace controller;


use Library\Controller;
use Library\DbConnection;
use Library\Request;
use Library\Session;
use model\UserModel;

class UserController extends Controller
{

	public function __construct(Request $request)
	{
		if ($request->isPost())
			$this->user_option($request);
		elseif ($hash = $request->get('email_confirm'))
			$this->email_confirm($request, $hash);
		else
		{
			$array['photo'] = $this->take_photo();
			$this->content = $this->render('../view/login_form.php', $array);
		}
	}

	public function __toString()
	{
		return $this->content;
	}

	public function user_option(Request $request)
	{
		$array = array();
		$user = new UserModel($request);
		if ($request->post('submit') == 'reg')
		{
			if (!$user->check_user())
			{
				$user->add_user();
				$user->send_mail($request);
				$array['page'] = "User added, but you must confirm your registration on email";
			}
			else
				$array['page'] = "User with this email exist!<br>";
		}
		elseif ($request->post('submit') == 'log')
		{
			if ($user->check_user() && $user->check_password())
			{
				Session::set('login', $user->email);
				Session::set('id', $user->id);
				$array['page'] = "Welcome Back!<br>";
			}
			else
				$array['page'] = "Incorrect password or email<br>";
		}
		elseif ($request->post('submit') == 'Send new password on email')
			$array['page'] = $this->forget_password($request, $user);
		elseif ($request->post('submit') == 'logout') {
			unset($_SESSION['login']);
			$array['page'] = "You are logout now!";
		}
		$this->content = $this->render('', $array);
	}

	public function forget_password(Request $request, UserModel $user)
	{
		if ($user->check_user())
		{
			$user->password = $user->randomPassword();
			$db = DbConnection::getInstance()->getPdo();
			$sth = $db->prepare('UPDATE user SET password = :password WHERE email = :email');
			$sth->execute(array('email' => $user->email, 'password' => hash('md5', $user->password)));
			mail($user->email, "Change password","Your new password is '$user->password', have fun!");
			return "See your mailbox for new password";
		}
		else
			return "User $user->email wasn't found!";

	}

	public function email_confirm(Request $request, $hash)
	{
		$email = "";
		$db = DbConnection::getInstance()->getPdo();
		$sth = $db->query('SELECT * FROM user');
		$sth->execute();
		$users = $sth->fetchAll(\PDO::FETCH_ASSOC);

		foreach ($users as $user)
		{
			if (hash('md5', $user['email']) == $hash)
				$email = $user['email'];
		}
		$array = array('check_email' => true,'email' => $email);
		$sth = $db->prepare('UPDATE user SET check_email = :check_email  WHERE email = :email');
		$sth->execute($array);
		$array['page'] = "Your registration confirm success!";
		$this->content = $this->render('', $array);
	}

	public function take_photo()
	{
		$img = array();
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->query("SELECt * FROM photo LIMIT 6");
		$ths->execute();
		$photo = $ths->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($photo as $item)
			$img[$item['id']] = $item['img'];
		return $img;
	}
}