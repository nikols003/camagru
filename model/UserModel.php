<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.04.2017
 * Time: 22:25
 */

namespace model;


use Library\DbConnection;
use Library\Request;
use Library\Session;

class UserModel
{
	public $First_Name;
	public $Last_Name;
	public $id;
	public $email;
	public $password;
	private $check_email;
	private $check_password;

	public function __construct(Request $request)
	{
		if ($request->post('First_Name'))
			$this->First_Name = $request->post('First_Name');
		if ($request->post('Last_Name'))
			$this->Last_Name = $request->post('Last_Name');
		$this->email = $request->post('email');
		if ($request->post('password'))
			$this->password = hash('md5', $request->post('password'));
	}

	static function take_all()
	{
		$db = DbConnection::getInstance()->getPdo();
		$sth = $db->query('SELECT * FROM user');
		$sth->execute();
		return $sth->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function check_user()
	{
		$users = self::take_all();
		foreach ($users as $user)
			if ($user['email'] == $this->email)
			{
				$this->check_password = $user['password'];
				$this->check_email = $user['check_email'];
				$this->id = $user['id'];
				return (True);
			}
		return False;
	}

	public function send_mail(Request $request)
	{
		$port = $request->server('SERVER_PORT') ? ":".$request->server('SERVER_PORT') : "";
		$link = "http://".$request->server('SERVER_NAME').$port.$request->server('PHP_SELF')."?email_confirm=".hash
			('md5',
				$this->email);
		$message = "Hi, if you realy one second ago had registration on".$request->server('SERVER_NAME')." you must go by this link <br>".$link;
		mail($this->email, "Confirm your registration", $message);
	}

	public function check_password()
	{
		if ($this->password == $this->check_password && $this->check_email)
			return True;
		return False;
	}

	public function add_user()
	{
		$user = array('email' => $this->email, 'first_name' => $this->First_Name, 'last_name' => $this->Last_Name, 'password' => $this->password);
		$db = DbConnection::getInstance()->getPdo();
		$sth = $db->prepare('INSERT INTO `user` (email, first_name, last_name, password) VALUES (:email, :first_name, :last_name, :password)');
		$sth->execute($user);
	}

	static public function add_photo_to_user($file_name)
	{
		$db = DbConnection::getInstance()->getPdo();
		$sth = $db->prepare('SELECT * FROM photo WHERE user_id = :id');
		$sth->execute(array('id' => Session::get('id')));
		$user = $sth->fetchAll(\PDO::FETCH_ASSOC);
		$dir_name = preg_split('/@/', Session::get('login'));
		$file_name = "webroot/img/".$dir_name[0]."/".$file_name;
		foreach ($user as $item) {
			if ($item['img'] == $file_name)
				return "Image with this name already exist, try another!";
		}

		$array = array('img' => $file_name, 'id' => Session::get('id'));
		$sth = $db->prepare('INSERT INTO photo (user_id, img) VALUES (:id, :img)');
		$sth->execute($array);
		return "Img added";
	}

	static function randomPassword()
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
}