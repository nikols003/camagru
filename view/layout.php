<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Site</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link href="webroot/css/style.css" rel="stylesheet">
	<script src="webroot/js/capture.js"></script>
</head>

<body>

<div class="wrapper">

	<header class="header">
		<div class="header_button"><a href="/camagru/" >Home</a></div>
		<?php
		if (\Library\Session::has('login'))
			echo "<div class=\"header_button\"><a href=\"photo?user=".\Library\Session::get('login')."\">Your profile</a></div>";
		if (\Library\Session::has('login'))
			echo "<div class=\"header_button_right\"><form action='/camagru/user' method='post'><input type='submit' 
name='submit' id='but' value='logout'> </input></form></div>";
		?>
	</header><!-- .header-->

	<div class="middle">

		<div class="container">
			<main class="content">
				<?=$content?>
				<?php
				if (isset($big_photo) && is_array($big_photo))
				{
					foreach ($big_photo as $id => $img){
						echo "<div class='big_photo'>";
						if ($_GET['user'] == $_SESSION['login'])
						{
							echo <<<HTML
						<div style="position: absolute; width: 500px; padding-left: 235px">
							<form method="post" action="photo?user={$_GET['user']}">
								<input type="hidden" name="photo_id" value="{$id}">
								<input style="background: none ; width: 20px; height: 20px" type="image" 
								src="webroot/img/trash.png" 
								name="submit" value="delete_photo">
							</form>
						</div>
HTML;
						}
						echo "<img style='width: 500px; height: 500px' src=\"$img\" alt=\"photo\">";
						foreach ($comments as $comment)
							if ($comment['photo_id'] == $id)
							{
								echo "Author: ".$comment['author_name']."<br>".$comment['time']."<br>"
										.$comment['description'];
								if ($_SESSION['login'] == $comment['author_name'] || $_SESSION['login'] == $_GET['user'])
									echo <<<HTML
							<form method="post" action="photo?user={$_GET['user']}">
								<input type="hidden" name="comment_id" value="{$comment['id']}">
								<input style="background: none ;width: 20px; height: 20px" type="image" 
								src="webroot/img/delete.png" 
								name="submit" value="delete_comment">
							</form>
HTML;
								echo "<hr>";
							}
						$comment_form = <<<HTML
						<div style="width: 400px">
							<form method="post" action="photo?user={$_GET['user']}">
								<input type="text" name="description">
								<input type="hidden" name="photo_id" value="{$id}">
								<br>
								<input id="but" type="submit" name="submit" value="comment"><br>
								<a href="https://www.facebook.com/sharer/sharer.php?u=http://localhost:8080/camagru/photo" 
								target="_blank">
  <img src='webroot/img/share-on-facebook.png' style='margin-bottom: 5px; width: 180px; height: 30px' alt='share'></a>
							</form>
						</div>
HTML;
						echo $comment_form;
						echo "</div>";
					}
				}
				if (!isset($_GET['user']) && \Library\Session::has('login')) {
					?>
					<div class="radio_tool">
						<input size="18" name="a" type="radio" onclick="document.getElementById('222').src =
						'webroot/img/tools/1.gif'">
						<input name="a" type="radio" onclick="document.getElementById('222').src =
						'webroot/img/tools/4.png'">
						<input name="a" type="radio" onclick="document.getElementById('222').src =
						'webroot/img/tools/2.png'">
						<input name="a" type="radio" onclick="document.getElementById('222').src =
						'webroot/img/tools/3.png'">
						<input name="a" type="radio" onclick="document.getElementById('222').src =
						'webroot/img/tools/5.png'">
						<input name="a" type="radio" onclick="document.getElementById('222').src =
						'webroot/img/tools/6.png'">
					</div>
					<div class="contentarea">
						<div id="camera" class="camera">
							<video id="video">Video stream not available.</video>
							<img id="222" alt="">
							<button id="startbutton">Take photo</button>
						</div>
						<canvas style="display: none" id="canvas">
						</canvas>
						<div class="output">
							<form action="/camagru/" method="POST">
								<input id="send" type="hidden" name="photo" />
								<img id="photo" alt="The screen capture will appear in this box.">
								<button name="submit" type="submit" value="Send File" id="startbutton">Download</button>
							</form>
						</div>
					</div>
					<?php
				}
				?>

			</main><!-- .content -->
		</div><!-- .container-->

		<aside class="right-sidebar">
			<?php
			if (isset($photo) && is_array($photo)){
				foreach ($photo as $item)
					echo "<img src=\"$item\" alt=\"photo\">";}
			elseif (isset($photo))
				echo $photo;
			if (isset($users)) {
				echo "Other users:<br>";
				foreach ($users as $user)
					echo "<a href=\"photo?user=".$user['email']."\">".$user['first_name']."</a><br>";
			}?>

		</aside><!-- .right-sidebar -->

	</div><!-- .middle-->

</div><!-- .wrapper -->

<footer class="footer">
</footer>

</body>
</html>

