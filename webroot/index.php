<?php

	ini_set('display_errors', 1);
	error_reporting(E_ALL);



	spl_autoload_register(function ($name) {
		$name = str_replace('\\', '/', $name);
		require '../'.$name.'.php';});
	session_start();

	$content = '';
	new config\Setup();
	$request = new \Library\Request();

	if (!\Library\Session::has('login'))
		$content = new \controller\UserController($request);
	else
	{
		$action = str_replace('/camagru/', '', $request->getURI());
		$action = ucfirst($action);
		if ($action == "")
			$action = 'Photo';
		$action = "\\controller\\".$action."Controller";
		$action = str_replace('\\', '/', $action);
		if (!file_exists('../'.$action.".php"))
			$content = \Library\Controller::renderError("<h1 style='font-size: 25px; line-height: 30px'>$action doesn't exist!!!</h1>");
		else
		{
			$action = str_replace('/', '\\', $action);
			$content = new $action($request);
		}
	}
	echo $content;
?>